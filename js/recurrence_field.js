(function ($) {
/**
 * Initializes the recurrence input on recurrence fields.
 */
Drupal.behaviors.recurrenceField = {
  attach: function(context) {
    // Define a custom template for the display of recurring rules so that the
    // add and delete links are below the display.
    $.template(
      'displayTmpl', 
      [
        '<div class="ridisplay">',
          '<div class="rimain">',
            '<label class="ridisplay">${i18n.displayUnactivate}</label>',
            '{{if !readOnly}}',
              '<a href="#" name="riedit">${i18n.add_rules}</a>',
              '<a href="#" name="ridelete" style="display: none;">${i18n.delete_rules}</a>',
            '{{/if}}',
          '</div>',
          '<div class="rioccurrences" style="display: none;" /></div>',
        '</div>'
      ].join('\n')
    );
    
    // Redefine the i18n text for the widget. We need to redefine everything
    // just to change a few items.
    $.tools.recurrenceinput.localize('en', {
      title: 'Recurrence',
      displayUnactivate: 'Does not repeat',
      displayActivate: 'Repeats every',
      add_rules: 'Edit',
      edit_rules: 'Edit',
      delete_rules: 'Clear',
      add:  'Add',
      refresh: 'Refresh',
      preview: 'Selected dates',
      addDate: 'Add date',
      recurrenceType: 'Repeats:',
      dailyInterval1: 'Repeat every:',
      dailyInterval2: 'days',
      weeklyInterval1: 'Repeat every:',
      weeklyInterval2: 'week(s)',
      weeklyWeekdays: 'Repeat on:',
      weeklyWeekdaysHuman: 'on:',
      monthlyInterval1: 'Repeat every:',
      monthlyInterval2: 'month(s)',
      monthlyDayOfMonth1: 'Day',
      monthlyDayOfMonth1Human: 'on day',
      monthlyDayOfMonth2: 'of the month',
      monthlyDayOfMonth3: 'month(s)',
      monthlyWeekdayOfMonth1: 'The',
      monthlyWeekdayOfMonth1Human: 'on the',
      monthlyWeekdayOfMonth2: '',
      monthlyWeekdayOfMonth3: 'of the month',
      monthlyRepeatOn: 'Repeat on:',
      yearlyInterval1: 'Repeat every:',
      yearlyInterval2: 'year(s)',
      yearlyDayOfMonth1: 'Every',
      yearlyDayOfMonth1Human: 'on',
      yearlyDayOfMonth2: '',
      yearlyDayOfMonth3: '',
      yearlyWeekdayOfMonth1: 'The',
      yearlyWeekdayOfMonth1Human: 'on the',
      yearlyWeekdayOfMonth2: '',
      yearlyWeekdayOfMonth3: 'of',
      yearlyWeekdayOfMonth4: '',
      yearlyRepeatOn: 'Repeat on:',
      range: 'End recurrence:',
      rangeNoEnd: 'Never',
      rangeByOccurrences1: 'After',
      rangeByOccurrences1Human: 'ends after',
      rangeByOccurrences2: 'occurrence(s)',
      rangeByEndDate: 'On',
      rangeByEndDateHuman: 'ends on',
      including: ', and also',
      except: ', except for',
      cancel: 'Cancel',
      save: 'Save',
      recurrenceStart: 'Start of the recurrence',
      additionalDate: 'Additional date',
      include: 'Include',
      exclude: 'Exclude',
      remove: 'Remove',
      orderIndexes: ['first', 'second', 'third', 'fourth', 'last'],
      months: [
        'January', 'February', 'March', 'April', 'May', 'June',
        'July', 'August', 'September', 'October', 'November', 'December'
      ],
      shortMonths: [
        'Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun',
        'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'
      ],
      weekdays: [
        'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday',
        'Friday', 'Saturday'
      ],
      shortWeekdays: ['Sun', 'Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat'],
      longDateFormat: 'mmmm dd, yyyy',
      shortDateFormat: 'mm/dd/yyyy',
      unsupportedFeatures: 
        'Warning: This event uses recurrence features not ' +
        'supported by this widget. Saving the recurrence ' +
        'may change the recurrence in unintended ways:',
      noTemplateMatch: 'No matching recurrence template',
      multipleDayOfMonth: 'This widget does not support multiple days in monthly or yearly recurrence',
      bysetpos: 'BYSETPOS is not supported',
      noRule: 'No RRULE in RRULE data',
      noRepeatEvery: 'Error: The "Repeat every"-field must be between 1 and 1000',
      noEndDate: 'Error: End date is not set. Please set a correct value',
      noRepeatOn: 'Error: "Repeat on"-value must be selected',
      pastEndDate: 'Error: End date cannot be before start date',
      noEndAfterNOccurrences: 'Error: The "After N occurrences"-field must be between 1 and 1000',
      alreadyAdded: 'This date was already added',
      rtemplate: {
        daily: 'Daily',
        mondayfriday: 'Monday and Friday',
        weekdays: 'Weekday',
        weekly: 'Weekly',
        monthly: 'Monthly',
        yearly: 'Yearly'
      }
    });

    // Initialize the recurrence widget.
    $('.field-widget-recurrence-field-widget-default .recurrence-field', context).recurrenceinput({
      formOverlay: {
        onBeforeLoad: function(e) {
          var conf = this.getConf(),
            overlay = this.getOverlay()
            $window = $(window);

          // Calculate the top and left coordinates to center the overlay
          // since jquery.tools can not do so properly in jquery 1.8.
          conf.top = Math.max(($window.height() - overlay.outerHeight(true)) / 2, 0)
          conf.left = Math.max(($window.width() - overlay.outerWidth(true)) / 2, 0)
        }
      }
    });

    /**
     * Updates the position of the date input calendar.
     */
    var dateInputPosition = function() {
      var $dateInput = $('input[name=rirangebyenddatecalendar]'),
        position = $dateInput.offset();
  
      $dateInput.data('dateinput').getCalendar().css({
        top: position.top + $dateInput.outerHeight(true),
        left: position.left
      });
    };

    // Update the position of the calendar whenever it is displayed or the page
    // scrolls.
    $('input[name=rirangebyenddatecalendar]').bind('onShow', dateInputPosition);
    $(window).bind('scroll', dateInputPosition);
  }
};
})(jQuery);
